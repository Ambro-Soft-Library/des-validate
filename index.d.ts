/**
 * The method must call when library is used on client side.
 * @param {Function} translateFn - Translation function for errors. Given key and return translated message.
 * @param {Object} translatorKeys - Object that contains translator bundle keys for appropriate rule errors messages.
 */
export default function (translateFn: Function): object


/**
 * The method must call when library is used on server side.
 */
export function desValidateServer(): object