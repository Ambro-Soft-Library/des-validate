const rules = require('rules')

const errorCodes = {
  required: 'validation.rules._required',
  nameEnDe: 'validation.rules._nameEnDe',
  enDigitDash: 'validation.rules._enDigitDash',
  beginsWithEnDeLetter: 'validation.rules._beginsWithEnDeLetter',
  zip: 'validation.rules._zip',
  link: 'validation.rules._link',
  descripEnDe: 'validation.rules._descripEnDe',
  
  onlyDigits: 'validation.rules._only_digits',
  onlyGeorgianLetters: 'validation.rules._only_georgian_letters',
  email: 'validation.rules._email',
  phone: 'validation.rules._phone',
  latinCode: 'validation.rules._latinCode',
  lengthMin: 'validation.rules._lengthMin',
  lengthMax: 'validation.rules._lengthMax',
  positive: 'validation.rules._positive',
  verificationCode: 'validation.rules._verificationCode',
  requiredEnDe: 'validation.rules._requiredEnDe',
  requiredEmail: 'validation.rules._requiredEmail',
  requiredNickname: 'validation.rules._requiredNickname'
}

const validate = (translateFn) => {
  const sr = rules(translateFn, errorCodes)
  return {
    sr: sr,
    required: [sr.required],
    name: [sr.nameEnDe, sr.beginsWithEnDeLetter, sr.lengthMin({ value: 2 })],
    email: [sr.email],
    phone: [sr.phone],
    link: [sr.link],
    nickName: [sr.enDigitDash, sr.beginsWithEnDeLetter, sr.lengthMin({ value: 4 }), sr.lengthMax({ value: 20 })],
    nickname: [sr.enDigitDash, sr.beginsWithEnDeLetter, sr.lengthMin({ value: 4 }), sr.lengthMax({ value: 20 })],
    password: [sr.lengthMin({ value: 6 }), sr.lengthMax({ value: 64 })],
    descrip: [sr.descripEnDe, sr.lengthMin({ value: 2 })],
    zip: [sr.zip],
    verificationCode: [sr.numberMin({ value: 1000 }), sr.numberMax({ value: 9999 })],
    positive: [sr.positive],
    vinLasts: [sr.lengthMin({ value: 6 }), sr.lengthMax({ value: 6 })],
    // Temporary olds:
    requiredEnDe: [sr.required, sr.descripEnDe],
    requiredEmail: [sr.required, sr.email],
    requiredNickname: [sr.required, sr.latinCode, sr.lengthMin({ value: 4 }), sr.lengthMax({ value: 15 })]
  }
}

module.exports = validate
